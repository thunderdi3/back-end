package com.mitocode.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mitocode.model.Signos;

public interface ISignosService {

	int registrar(Signos signos);

	int modificar(Signos signos);

	void eliminar(int idSignos);

	Signos listarId(int idSignos);

	List<Signos> listar();
	
	Page<Signos> listAllByPage(Pageable pageable);
	
	int getUltimo();
	
}
