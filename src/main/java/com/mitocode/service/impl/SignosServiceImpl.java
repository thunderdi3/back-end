package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mitocode.dao.ISignosDAO;
import com.mitocode.model.Signos;
import com.mitocode.service.ISignosService;

@Service
public class SignosServiceImpl implements ISignosService{

	@Autowired
	private ISignosDAO dao;
	
	@Override
	public int registrar(Signos signos) {
		int rpta = 0;
		rpta = dao.save(signos) != null ? signos.getIdSignos(): 0;
		return rpta > 0 ? 1 : 0;
	}

	@Override
	public int modificar(Signos signos) {
		int rpta = 0;
		rpta = dao.save(signos) != null ? signos.getIdSignos() : 0;
		return rpta > 0 ? 1 : 0;
	}

	@Override
	public void eliminar(int idSignos) {
		dao.delete(idSignos);
	}

	@Override
	public Signos listarId(int idSignos) {
		return dao.findOne(idSignos);
	}

	@Override
	public List<Signos> listar() {
		return dao.findAll();
	}

	@Override
	public Page<Signos> listAllByPage(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	public int getUltimo() {
		return dao.getUltimoReg();
	}

}
