package com.mitocode.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mitocode.model.Signos;

@Repository
public interface ISignosDAO extends JpaRepository<Signos, Integer> {

	@Query(value = "select (id_signos+1) from signos order by 1 desc limit 1", nativeQuery = true)
	int getUltimoReg();

}
